# Create Tenant1
resource "aci_tenant" "tenant1_tn" {
    name = "Tenant1_TN"  
}

# Create Tenant1 AppProfile 1
resource "aci_application_profile" "tenant1_app1_ap" {
    tenant_dn = aci_tenant.tenant1_tn.id
    name = "Tenant1_App1_AP"
}

# Create Tenant1 App1 EPG
resource "aci_application_epg" "tenant1_app1_epg" {
    application_profile_dn = aci_application_profile.tenant1_app1_ap.id
    name = "Tenant1_App1_EPG"  
}

# Create Tenant1 App1 BD
resource "aci_bridge_domain" "tenant1_app1_bd" {
    tenant_dn = aci_tenant.tenant1_tn.id
    name = "Tenant1_App1_BD"
}

# Create Tenant1 App1 VRF
resource "aci_vrf" "tenant1_app1_vrf" {
    tenant_dn = aci_tenant.tenant1_tn.id
    name = "Tenant1_App1_VRF"  
}

# Create Tenant1 App1 Subnet
resource "aci_subnet" "tenant1_app1_sn" {
    parent_dn = aci_bridge_domain.tenant1_app1_bd.id
    ip = "1.1.1.1/24"  
}