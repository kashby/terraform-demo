# Create Tenant2
resource "aci_tenant" "tenant2_tn" {
    name = "Tenant2_TN"  
}

# Create Tenant2 AppProfile 1
resource "aci_application_profile" "tenant2_app1_ap" {
    tenant_dn = aci_tenant.tenant2_tn.id
    name = "Tenant2_App1_AP"
}

# Create Tenant2 App1 EPG
resource "aci_application_epg" "tenant2_app1_epg" {
    application_profile_dn = aci_application_profile.tenant2_app1_ap.id
    name = "Tenant2_App1_EPG"  
}

# Create Tenant2 App1 BD
resource "aci_bridge_domain" "tenant2_app1_bd" {
    tenant_dn = aci_tenant.tenant2_tn.id
    name = "Tenant2_App1_BD"
}

# Create Tenant2 App1 VRF
resource "aci_vrf" "tenant2_app1_vrf" {
    tenant_dn = aci_tenant.tenant2_tn.id
    name = "Tenant2_App1_VRF"  
}

# Create Tenant2 App1 Subnet
resource "aci_subnet" "tenant2_app1_sn" {
    parent_dn = aci_bridge_domain.tenant2_app1_bd.id
    ip = "1.1.1.1/24"  
}