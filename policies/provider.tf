# Initialize ACI Provider
terraform {
    required_providers {
        aci = {
            source = "CiscoDevNet/aci"
        }
    }
}