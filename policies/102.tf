# Create Leaf Switch Profile
resource "aci_leaf_profile" "lsp_102" {
    name = "102_LSP"
    leaf_selector {
        name = "102"
        switch_association_type = "range"
        node_block {
            name  = "102"
            from_ = "102"
            to_   = "102"
        }
    }
}