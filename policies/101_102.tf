# Create VPC Pair
resource "aci_vpc_explicit_protection_group" "vpc_101_102" {
  name = "101_102"
  switch1 = "101"
  switch2 = "102"
}

# Create Leaf Switch Profile
resource "aci_leaf_profile" "lsp_101_102" {
    name = "101_102_LSP"
    leaf_selector {
        name = "102"
        switch_association_type = "range"
        node_block {
            name  = "101_102"
            from_ = "101"
            to_   = "102"
        }
    }
}