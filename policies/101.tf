# Create Leaf Switch Profile
resource "aci_leaf_profile" "lsp_101" {
    name = "101_LSP"
    leaf_selector {
        name = "101"
        switch_association_type = "range"
        node_block {
            name  = "101"
            from_ = "101"
            to_   = "101"
        }
    }
}