# Initialize ACI Providers
terraform {
    required_providers {
        aci = {
            source = "CiscoDevNet/aci"
        }
    }
}

# Configure ACI Provider
provider "aci" {
    username = var.user.username
    password = var.user.password
    url = var.user.url
}

# Include Tenant1 Directory
module "tenant1" {
    source = "./tenant1"
}

# Include Tenant2 Directory
module "tenant2" {
    source = "./tenant2"
}

# Include Policies Directory
module "policies" {
    source = "./policies"
}